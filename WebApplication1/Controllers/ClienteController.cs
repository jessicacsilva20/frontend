﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class ClienteController : Controller
    {
        public IActionResult Index()
        {
            var model = new ClienteViewModel();
            model.DataNascimento = DateTime.Now;
            return View(model);
        }
    }
}