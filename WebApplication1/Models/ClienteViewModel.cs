﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class ClienteViewModel
    {
        public int Id { get; set; }

        public string Nome { get; set; }

        public int Idade { get; set; }

        
        [DisplayFormat(DataFormatString = "{0:yyyy-MM-dd}")]
        public DateTime DataNascimento { get; set; }

    }
}
